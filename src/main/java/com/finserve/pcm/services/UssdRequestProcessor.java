/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.pcm.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finserve.pcm.EquitelPleaseCallMeApplication;
import com.finserve.pcm.MainController;
import com.finserve.pcm.pojos.MessageConsumer;
import com.finserve.pcm.pojos.UssdRequest;
import com.finserve.pcm.repositories.PcmMessagesRepository;
import com.finserve.pcm.utils.UssdUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class UssdRequestProcessor {
    
    private RabbitTemplate rabbitTemplate;
    private MessageConsumer consumer;
    private ConfigurableApplicationContext context;
    
    static Log log = LogFactory.getLog(MainController.class.getName());
    
    @Autowired
    UssdUtils ussdUtils;
    
    @Autowired
    Environment env;
    
    @Autowired
    PcmMessagesRepository pcmMessageRepository;
    
    //Validatation
    //1. Check if input is correct
    //2. Check id requestor = recipient
    //3. Check PCM count today
    //4. Publish to Queue
    //5. Respond to Requestor
    
    public UssdRequestProcessor(MessageConsumer consumer, RabbitTemplate rabbitTemplate,
            ConfigurableApplicationContext context) {
        this.consumer = consumer;
        this.rabbitTemplate = rabbitTemplate;
        this.context = context;
    }
    
    public UssdRequest requestHandler(UssdRequest request){
        
        String[] input = request.getInput().split("\\*");
        if(input.length > 1){
           String recipientMsisdn = ussdUtils.validateMsisdn(input[1]);
           Long messageCount = pcmMessageRepository.getMessageCount(request.getMsisdn());
           log.info("MessageCount:"+messageCount);
            if(recipientMsisdn == null){
                //Invalid input
                request.setResponseMessage(env.getProperty("pcm.message.wrongformat"));
            }else {
                request.setRecipient(recipientMsisdn);
                if(recipientMsisdn.equals(request.getMsisdn())){
                    //Sender is same as recipient
                    request.setResponseMessage(env.getProperty("pcm.message.samemsisdn"));
                }else{
                    if(messageCount > 5){
                        //Daily limit exceeded
                        request.setResponseMessage(env.getProperty("pcm.message.overlimit"));
                    }else{
                        //Success
                        request.setMessage(env.getProperty("pcm.message"));
                        request.setResponseMessage(String.format(env.getProperty("pcm.message.success"),recipientMsisdn));
                        //Publish to queue
                        //rabbitTemplate.convertAndSend("pcm-queue", request.getMessage());
                        //POJO to JSON
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            String requestJson = mapper.writeValueAsString(request);
                            rabbitTemplate.convertAndSend(EquitelPleaseCallMeApplication.queueName, requestJson);
                        } catch (JsonProcessingException ex) {
                            log.error(ex);
                        }
                        
                    }
                    
                }
            } 
        }else{
            //Invalid input
            request.setResponseMessage(env.getProperty("pcm.message.wrongformat"));
        }
        
        
        return request;
    }
    
}
