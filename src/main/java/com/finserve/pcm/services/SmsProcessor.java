/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.pcm.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finserve.pcm.MainController;
import com.finserve.pcm.models.PcmMessages;
import com.finserve.pcm.pojos.UssdRequest;
import com.finserve.pcm.repositories.PcmMessagesRepository;
import com.finserve.pcm.utils.RpcUtils;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */

//@Service
public class SmsProcessor implements Runnable{
    
    static Log log = LogFactory.getLog(SmsProcessor.class.getName());
    
    PcmMessagesRepository pcmMessagesRepository;
    
    RpcUtils rpcUtils;
    
    String message;

    public SmsProcessor(PcmMessagesRepository pcmMessagesRepository, RpcUtils rpcUtils, String message) {
        this.pcmMessagesRepository = pcmMessagesRepository;
        this.rpcUtils = rpcUtils;
        this.message = message;
    }

    @Override
    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        UssdRequest pcmReq = null;
        
        try {
            pcmReq = mapper.readValue(message, UssdRequest.class);

            PcmMessages msg = new PcmMessages();
            msg.setMessage(pcmReq.getMessage());
            msg.setSenderMsisdn(pcmReq.getMsisdn());
            msg.setRecipientMsisdn(pcmReq.getRecipient());
            msg.setSmsSuccess(Boolean.FALSE);
            msg.setSendTime(Calendar.getInstance().getTime());
            
            Long id = Thread.currentThread().getId();
            
            log.info("CurrentThreadIDL"+id);

            pcmMessagesRepository.save(msg);

            String result = rpcUtils.callSMSC(pcmReq);
            
            if(result.equals("0")){
                msg.setSmsSuccess(Boolean.TRUE);
                pcmMessagesRepository.save(msg);
            }else{
                msg.setSmsSuccess(Boolean.FALSE);
                pcmMessagesRepository.save(msg);
            }

        } catch (IOException ex) {
            log.error(ex);
        }
    }
}
