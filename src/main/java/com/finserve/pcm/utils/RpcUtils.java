/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.pcm.utils;

import com.finserve.pcm.pojos.UssdRequest;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Wambayi
 */

@Service
public class RpcUtils {
    
    static Log log = LogFactory.getLog(UssdUtils.class.getName());
    
    @Autowired
    Environment env;
    
    PoolingHttpClientConnectionManager connManager;
    CloseableHttpClient client;

    public RpcUtils() {
        this.connManager = new PoolingHttpClientConnectionManager();
        connManager.setDefaultMaxPerRoute(5);
        connManager.setMaxTotal(5);
        this.client = HttpClients.custom()
            .setConnectionManager(connManager)
            .build();
    }
    
    public String callSMSC(UssdRequest pcmRequest){
        String txnStatusCode = null;
        
        String smscXML = "<Request>\n" +
                "<username>"+env.getProperty("smsc.username")+"</username>\n" +
                "<password>"+env.getProperty("smsc.password")+"</password>\n" +
                "<commandId>sendSMS</commandId>\n" +
                "<oa>"+pcmRequest.getMsisdn()+"</oa>\n" +
                "<da>"+pcmRequest.getRecipient()+"</da>\n" +
                "<seqno>"+pcmRequest.getRequestId()+"</seqno>\n" +
                "<ud>"+pcmRequest.getMessage()+"</ud>\n" +
                "<pid>0</pid>\n" +
                "<servicetype>CMT</servicetype>\n" +
                "<validityperiod>100</validityperiod>\n" +
                "<registerdelivery>0</registerdelivery>\n" +
                "<dlruri>http://10.0.0.250:8765/DeliveryResp</dlruri>\n" +
            "</Request>";
        String smscUrl = env.getProperty("smsc.url");
        try {
            HttpPost httpPost = new HttpPost(smscUrl);
            httpPost.setEntity(new StringEntity(smscXML));
        
            HttpResponse httpResponse = client.execute(httpPost);
            HttpEntity entity = httpResponse.getEntity();
            
            StatusLine respCode = httpResponse.getStatusLine();
            String smscResp = EntityUtils.toString(entity);
            
            log.info("Status:"+respCode.getStatusCode()+"| Message:"+smscResp);
            
            if(respCode.getStatusCode() == 200){
                InputSource source = new InputSource(new StringReader(smscResp.toString()));
            
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(source);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("Response");
                Element eElement = (Element) nList.item(0);

                txnStatusCode = eElement.getElementsByTagName("status").item(0).getTextContent();
            }else{
                txnStatusCode = "1";
            }      
        } catch (UnsupportedEncodingException ex) {
            log.error(ex);
        } catch (IOException ex) {
            log.error(ex);
        } catch (ParserConfigurationException ex) {
            log.error(ex);
        } catch (SAXException ex) {
            log.error(ex);
        }
        
        return txnStatusCode;
    }
    
}
