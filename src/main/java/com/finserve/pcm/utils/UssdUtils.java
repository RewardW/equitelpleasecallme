/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.pcm.utils;

import com.finserve.pcm.pojos.UssdRequest;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Wambayi
 */

@Service
public class UssdUtils {
    
    static Log log = LogFactory.getLog(UssdUtils.class.getName());
    
    public UssdRequest parseUssdReq(String req){
        
        InputSource source = new InputSource(
                new StringReader(req.toString()
                        .replace("\n", "")
                        .replace("\r", "")
                        .replace("\t", "")
                        .replace(" ", "")));
        
        UssdRequest newReq = new UssdRequest();
        
        try{

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(source);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("USSDDynMenuRequest");
            Element eElement = (Element) nList.item(0);
            
            newReq.setRequestId(eElement.getElementsByTagName("requestId").item(0).getTextContent());
            newReq.setMsisdn(eElement.getElementsByTagName("msisdn").item(0).getTextContent());
            newReq.setTimestamp(eElement.getElementsByTagName("timeStamp").item(0).getTextContent());
            newReq.setStarCode(eElement.getElementsByTagName("starCode").item(0).getTextContent());
            
            /*NodeList status = eElement.getElementsByTagName("dataSet");
            Element statusParams =  (Element) status.item(0);
            String statusCode = statusParams.getElementsByTagName("status").item(0).getTextContent();
            String responseCode = statusParams.getElementsByTagName("responseCode").item(0).getTextContent();
            */
            NodeList dataSet = eElement.getElementsByTagName("dataSet");
            Element dataSetElement =  (Element) dataSet.item(0);
            
            NodeList options=dataSetElement.getChildNodes();
            
                    
            for(int i = 0;i < options.getLength();i++){
                
                Node n = options.item(i);
                System.out.println("index="+i+" | NodeType:"+n.getNodeType());
                if (n.getNodeType() == Node.ELEMENT_NODE)
                {
                    String text=n.getTextContent();
                    System.out.println(text);
                }
                log.debug("ID:"+options.item(i).getTextContent()); //.getElementsByTagName("id").item(0).getTextContent());
                log.debug("Value:"+options.item(i).getTextContent()); //getElementsByTagName("value").item(0).getTextContent());
            }
            
            Element option = (Element) options.item(4);
            
            //Node option =   .item(0);
            //System.out.println(option.getElementsByTagName("id").item(0).getTextContent());
            //System.out.println(option.getElementsByTagName("value").item(0).getTextContent());
            newReq.setInput(option.getElementsByTagName("value").item(0).getTextContent());
            
        }catch(Exception e){
            log.error("parseUSSD: "+e);
            e.printStackTrace();
            newReq.setMessage("Please try again later.");
            
        }
        return newReq;
    }
    
    public String ussdResponse(UssdRequest resp){
        String responseString = "<USSDDynMenuResponse>\n" +
            "	<requestId>"+resp.getRequestId()+"</requestId>\n" +
            "	<msisdn>"+resp.getMsisdn()+"</msisdn>\n" +
            "	<starCode>"+resp.getStarCode()+"</starCode>\n" +
            "	<langId>1</langId>\n" +
            "	<encodingScheme>0</encodingScheme>\n" +
            "	<dataSet>\n" +
            "		<param>\n" +
            "			<id>1</id>\n" +
            "		    <value>" + resp.getResponseMessage()+ "</value>\n" +
            "			<rspFlag>1</rspFlag>\n" +
            "			<rspTag>NATIONALID</rspTag>\n" +
            "			<rspURL>http://10.1.9.57:9090/6dussdc</rspURL>\n" +
            "			<appendIndex>0</appendIndex>\n" +
            "			<default>1</default>\n" +
            "		</param>\n" +
            "	</dataSet>\n" +
            "	<ErrCode>1</ErrCode>\n" +
            "   <errURL>http://10.1.9.57:9090/6dussdc/errcallback</errURL>\n" +
            "	<timeStamp>"+resp.getTimestamp()+"</timeStamp>\n" +
            "</USSDDynMenuResponse> ";
        
        return responseString;
    }
    /*
    public UssdRequest parseUssdDom4j(String req){
        SAXReader reader = new SAXReader();
        try {
            Document document = (Document) reader.read(req);
            Node
            
        } catch (DocumentException ex) {
            Logger.getLogger(UssdUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    public String validateMsisdn(String msisdn){
        String newMsisdn;
        //254763100100 || 0763100100
        if(msisdn.length() == 12 && msisdn.startsWith("2")){
            newMsisdn = msisdn;
        }else if(msisdn.length() == 10 && msisdn.startsWith("0")){
            newMsisdn = "254"+msisdn.substring(1, msisdn.length());
        }else{
            newMsisdn = null;
        }
        
        return newMsisdn;
    }
    
}
