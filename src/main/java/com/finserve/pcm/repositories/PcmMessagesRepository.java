package com.finserve.pcm.repositories;


import com.finserve.pcm.models.PcmMessages;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wambayi
 */
public interface PcmMessagesRepository extends PagingAndSortingRepository<PcmMessages, Long> {
    
    // using this query in qualificationCheck, no transaction required
    //@Query("select c from Customer c where subscriberNumber = :msisdn")
    @Query("select count(*) from PcmMessages where senderMsisdn = :msisdn and "
            + "sendTime between CONCAT(CURDATE(),' 00:00:00') and CONCAT(CURDATE(),'  23:59:59') ")
    Long getMessageCount(@Param("msisdn") String msisdn);
    
}
