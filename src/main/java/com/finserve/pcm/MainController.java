package com.finserve.pcm;

import com.finserve.pcm.pojos.UssdRequest;
import com.finserve.pcm.services.UssdRequestProcessor;
import com.finserve.pcm.utils.UssdUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pcm")
public class MainController {
    
        @Autowired
        UssdUtils ussdUtils;
        
        @Autowired
        UssdRequestProcessor ussdRequestProcessor;
	
	static Log log = LogFactory.getLog(MainController.class.getName());
	
	/*@RequestMapping(value = "/send/{sender}/{mgw}", method = RequestMethod.GET)
	public String receiveRequest(@PathVariable String sender,@PathVariable String req){
		log.info("newReq(sender:"+req+",msg:"+req);
	}*/
        
        @RequestMapping(value = "/ussd", method = RequestMethod.POST, consumes = {"application/xml","text/html" }, 
                produces = { "application/xml","text/html" })
        public String ussd(@RequestBody String req){
            UssdRequest request = ussdUtils.parseUssdReq(req);
            
            log.info("Params: "+request.toString());
            
            return ussdUtils.ussdResponse(ussdRequestProcessor.requestHandler(request));
        }

}
