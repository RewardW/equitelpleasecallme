/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finserve.pcm.pojos;

import com.finserve.pcm.repositories.PcmMessagesRepository;
import com.finserve.pcm.services.SmsProcessor;
import com.finserve.pcm.utils.RpcUtils;
import com.finserve.pcm.utils.UssdUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wambayi
 */
@Component
public class MessageConsumer {
    
    //@Autowired
    //SmsProcessor smsProcessor;
    
    @Autowired
    PcmMessagesRepository pcmMessagesRepository;
    
    @Autowired
    RpcUtils rpcUtils;
    
    static Log log = LogFactory.getLog(UssdUtils.class.getName());
    
    public void receiveMessage(String message){
        log.info("Received Message: "+message);
        SmsProcessor smsProcessor = new SmsProcessor(pcmMessagesRepository,rpcUtils,message);
        Thread newThread = new Thread(smsProcessor, "smsProcessor");
        newThread.start();
    }
}
